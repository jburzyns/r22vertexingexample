# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# A script for re-running primary vertex finding on AOD
#
# Authors: Guglielmo Frattari & Jackson Burzynski

# python includes
import argparse
import glob
import sys

# Athena includes
from AthenaCommon.Logging import logging
from AthenaConfiguration.ComponentAccumulator import printProperties
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
from TrkConfig.VertexFindingFlags import VertexSetup
from AthenaConfiguration.AllConfigFlags import initConfigFlags
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.MainServicesConfig import MainServicesMiniCfg
from AthenaConfiguration.MainServicesConfig import MessageSvcCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from BeamSpotConditions.BeamSpotConditionsConfig import BeamSpotCondAlgCfg
from InDetConfig.InDetPriVxFinderConfig import primaryVertexFindingCfg

def runVertexing():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='Input file name', required=True)
    parser.add_argument('-o', '--output', help='Output file name', default="myAOD.pool.root")
    args = parser.parse_args()

    flags = initConfigFlags()
    flags.Detector.GeometryCalo = False
    flags.Detector.GeometryMuon = False
    flags.Exec.MaxEvents = 10
    flags.Input.Files = glob.glob(args.input)
    
    acc = MainServicesCfg(flags)
    acc.merge(PoolReadCfg(flags))
    
    # Configure primary vertex reco here, e.g.
    # flags.Tracking.PriVertex.setup = VertexSetup.ActsGaussAMVF
    # flags.Tracking.PriVertex.useBeamConstraint = False
    
    flags.Output.AODFileName = args.output
    flags.dump()
    flags.lock()

    acc.merge(BeamSpotCondAlgCfg(flags)) # To produce InDet::BeamSpotData CondHandle

    # add the algorithm to the configuration

    acc.merge(primaryVertexFindingCfg(flags, vxCandidatesOutputName="MyPrimaryVertices"))
    mlog = logging.getLogger("primaryVertexFindingConfigTest")
    mlog.info("Configuring  primaryVertexFinding: ")
    printProperties(
      mlog,
      acc.getEventAlgo("InDetPriVxFinder"),
      nestLevel=2,
      printDefaults=True,
    )
    # debug printout
    acc.printConfig(withDetails=True, summariseProps=True)
    
    #Adding the output to the AOD file
    inputList = []
    inputList.append("xAOD::ElectronContainer#Electrons")
    inputList.append("xAOD::ElectronAuxContainer#*")
    inputList.append("xAOD::PhotonContainer#Photons")
    inputList.append("xAOD::PhotonAuxContainer#*")
    inputList.append("xAOD::MuonContainer#Muons")
    inputList.append("xAOD::MuonAuxContainer#*")
    inputList.append("xAOD::TauJetContainer#*")
    inputList.append("xAOD::TauJetAuxContainer#*")
    inputList.append("xAOD::TruthVertexContainer#*")
    inputList.append("xAOD::TruthVertexAuxContainer#*")
    inputList.append("xAOD::VertexContainer#*")
    inputList.append("xAOD::VertexAuxContainer#*")
    inputList.append("xAOD::TrackParticleContainer#*")
    inputList.append("xAOD::TrackParticleAuxContainer#*")
    inputList.append("xAOD::TruthParticleContainer#*")
    inputList.append("xAOD::TruthParticleAuxContainer#*")
    inputList.append("xAOD::TruthEventContainer#*")
    inputList.append("xAOD::TruthEventAuxContainer#*")
    inputList.append("xAOD::EventInfo#EventInfo")
    inputList.append("xAOD::EventAuxInfo#EventInfoAux")
    
    acc.merge(OutputStreamCfg(flags, 'AOD', ItemList=inputList))
    
    # run the job
    status = acc.run()

    # report the execution status (0 ok, else error)
    sys.exit(not status.isSuccess())

if __name__ == "__main__":
  runVertexing()
